<?php
/**
 * @file
 * Views definitions for Multistep Profile2.
 */

/**
 * Implements hook_views_data().
 */
function multistep_profile2_views_data() {
  $data['multistep_profile2']['table']['group'] = t('Multistep Profile2');
  // This table references the {node} table.
  $data['multistep_profile2']['table']['join'] = array(
    'profile' => array(
      'left_field' => 'pid',
      'field' => 'pid',
      'extra' => "multistep_profile2.status = 'unsubmitted'",
    ),
  );
  // Status of a multistep form.
  $data['multistep_profile2']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status of the current step in the referenced Profile.'),
    'field' => array(
      'handler' => 'multistep_profile2_views_handler_field_status',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'multistep_profile2_views_handler_filter_status',
    ),
    'sort' => array(
      'handler' => 'multistep_profile2_views_handler_sort_status',
    ),
  );
  return $data;
}

/**
 * Implements hook_views_handlers().
 *
 * To register all of the basic handlers views uses.
 */
function multistep_profile2_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'multistep_profile2') . '/includes/views',
    ),
    'handlers' => array(
      // Field handlers.
      'multistep_profile2_views_handler_field_status' => array(
        'parent' => 'views_handler_field',
      ),
      // Sort handlers.
      'multistep_profile2_views_handler_sort_status' => array(
        'parent' => 'views_handler_sort',
      ),
      // Filter handlers.
      'multistep_profile2_views_handler_filter_status' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}
