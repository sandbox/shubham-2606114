<?php
/**
 * @file
 * Multistep Profile2 status field views handeler.
 */

/**
 * Field handler that returns the completion status of a multistep node.
 */
class multistep_profile2_views_handler_field_status extends views_handler_field {
  function query() {
    $this->ensure_my_table();
    $this->query->add_field('profile', 'type');
    $this->query->add_field('multistep_profile2', 'status');
    $this->query->add_groupby('pid');
  }

  function render($values) {
    dsm($values);
    if (!empty($values->multistep_profile2_status)) {
      return t('Incomplete');
    }
    if (variable_get('multistep_profile2_expose_' . $values->profile_type, 'enabled') == 'enabled') {
      return t('Complete');
    }
    return '';
  }

}
