
Summary
=======
Multistep Profile2 adds multiple-step functionality to profile2 type
editing forms. It does so by assigning a step number to each field
or field group within the profile2 type
and hiding all the fields or groups that do not belong to the current step. The
user can then use different submitting buttons that will redirect to the
previous, next, or current step.

The module also provides a block for each profile2 type with a menu of the
different groups within that form and a progress bar. This provides an easy way
to jump to different steps throughout the form without having to go one by one
as well as keeping track of the progress of the form.

For a full description visit the project page:
  http://drupal.org/project/multistep_profile2
  
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/multistep_profile2

Requirements
============
This module requires Fields, which is part of Drupal core. It also benefits
strongly from Field group, which can be found here:
  http://drupal.org/project/field_group

To Use
======
To use this module, go into the profile2 type editing form in
Structure >> Profile2 and select the Profile2 type you want to
enable Multistep for.
  
There will be a collapsed Multistep Form section below, mark it as Enabled and
enter the amount of steps that you want this form to span.
  
Now, whenever you add or edit a group (or a field that does not belong to any
group), you will be able to select which step that group belongs to. The group
will only be shown when in that step, or in all of them if All is selected as
an option.

Configuration
=============
To configure the multistep menu and the progress bar, go to
Administer >> Structure >> Blocks and configure the corresponding block that
will appear on the list. You can select whether to enable or disable the
menu and the progress bar.

To remove/show the Preview button on the profile2 editing form, go to
the profile2 type editing form in Administer >> Profile2 >> type and
check/uncheck the box that says "Hide Preview button".
