<?php

/**
 * @file
 * Administration settings for multistep.
 */

/**
 * Form used for administering multistep profile2 workflow settings.
 */
function multistep_profile2_admin_settings_workflow_form($form, &$form_state) {
  $form = array();
  $form['multistep_profile2_workflow_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Workflow mode'),
    '#description' => t('Disables navigation block links for steps that have not been submitted') . '<br /><strong>' . t('Note: This does not remove access from those steps, it merely displays the links as static text instead.') . '</strong>',
    '#options' => array(
      'free' => t('Steps can be submitted in any order (Default)'),
      'direct' => t('Steps can be submitted only in one direction (Wizard)'),
    ),
    '#default_value' => variable_get('multistep_profile2_workflow_mode', 'free'),
    '#weight' => -8,
  );
  $form['multistep_profile2_show_step_numbers'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show step numbers'),
    '#description' => t('Show step numbers in navigation block.'),
    '#default_value' => variable_get('multistep_profile2_show_step_numbers', FALSE),
    '#weight' => -6,
  );
  return system_settings_form($form);
}

/**
 * Form used for changing multistep profile2 form labels.
 */
function multistep_profile2_admin_settings_design_form($form, &$form_state) {
  drupal_set_message(t("Saving these strings will disable their translation via Drupal's language system. Use the reset button to return them to the original state."), 'warning');
  $form = array();
  $form['multistep_profile2_button_prev'] = array(
    '#type' => 'textfield',
    '#title' => t('Previous button'),
    '#description' => t('This button will bring you back one step. To omit this button, remove any text and leave this field blank.'),
    '#default_value' => variable_get('multistep_profile2_button_prev', t('< Previous')),
    '#weight' => -10,
  );
  $form['multistep_profile2_button_next'] = array(
    '#type' => 'textfield',
    '#title' => t('Next button'),
    '#description' => t('This button will move you forward one step.'),
    '#default_value' => variable_get('multistep_profile2_button_next', t('Next >')),
    '#required' => TRUE,
    '#weight' => -9,
  );
  $form['multistep_profile2_button_save'] = array(
    '#type' => 'textfield',
    '#title' => t('Save button'),
    '#description' => t('This button will save the form while remaining in the current step. To omit this button, remove any text and leave this field blank.'),
    '#default_value' => variable_get('multistep_profile2_button_save', t('Save')),
    '#weight' => -8,
  );
  $form['multistep_profile2_button_done'] = array(
    '#type' => 'textfield',
    '#title' => t('Done button'),
    '#description' => t('This button will save the form and redirect you to the node view.'),
    '#default_value' => variable_get('multistep_profile2_button_done', t('Done')),
    '#required' => TRUE,
    '#weight' => -7,
  );
  return system_settings_form($form);
}

/**
 * Form used for changing multistep profile2 form labels.
 */
function multistep_profile2_admin_settings_reset_form($form, &$form_state) {
  $form = array();
  $form['#description'] = t('This form will reset all the step data for the selected profile type. Use this if you just enabled multistep for a profile type that already contained profiles created.') . '<br /><strong>' . t('Warning! Resetting the tables for a profile type that already had multistep enabled could result in the loss of the step data!') . '</strong>';
  $options = array('' => '- Select -');
  foreach (profile2_get_types() as $type) {
    if (_multistep_profile2_is($type)) {
      $options[$type->type] = $type->label;
    }
  }
  $form['multistep_profile2_type'] = array(
    '#type' => 'select',
    '#title' => t('Profile type'),
    '#options' => $options,
    '#required' => TRUE,
    '#weight' => -1,
  );
  // Show reset button if multistep is enabled for this content type.
  $form['multistep_profile2_reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset data'),
    '#weight' => 1,
    '#access' => user_access('administer multistep profile2'),
  );
  return $form;
}

/**
 * Resets the multistep profile2 table data for the selected profile type.
 */
function multistep_profile2_admin_settings_reset_form_submit($form, &$form_state) {
  _multistep_profile2_reset_data($form_state['values']['multistep_profile2_type']);
  drupal_set_message(t('Data was reset successfully.'));
}
