<?php
/**
 * @file
 * Theme function for Multistep Profile2 module.
 */

/**
 * Display a navigation block.
 *
 * Use this theme function to override
 * the output / display of this block.
 */
function theme_multistep_profile2_navigation($variables = array()) {
  if (count($variables['links']['#children']) < 1) {
    return '';
  }
  $url = $next_unsubmitted_step = $label = $id = $class = $step = '';
  $content = array();
  foreach ($variables['links']['#children'] as $key => $group) {
    extract($group);
    if (isset($url) || $next_unsubmitted_step) {
      $options = array(
        'attributes' => array(
          'id' => $id,
          'class' => $class,
        ),
        'query' => array('step' => $step),
      );
      $content[$key] = l($label, $url, $options);
    }
    else {
      $content[$key] = '<span id="' . $id . '" class="' . implode(' ', $class) . '">' . $label . '</span>';
    }
  }
  return theme('item_list', array('items' => $content, 'type' => variable_get('multistep_profile2_show_step_numbers') ? 'ol' : 'ul'));
}

/**
 * Block Theme function that displays the default output of a progress bar.
 *
 * Use this theme function to override the output / display of this block.
 * For small prograss values < 50% the percentage is shown to the right.
 */
function theme_multistep_profile2_progress_bar($variables = array()) {
  list($left, $right) = array($variables['progress'] . '%', '');
  if ($variables['progress'] < 50) {
    list($left, $right) = array($right, $left);
  }
  $output = '<div class="multistep-profile2-progress-bar-wrapper">';
  $output .= '<div class="multistep-profile2-progress-bar multistep-profile2-progress-bar-' . $variables['progress'] . '" style="width: ' . $variables['progress'] . '%">' . $left . '</div>';
  if ($right) {
    $output .= '<div class="multistep-profile2-progress-bar-percentage-right">' . $right . '</div>';
  }
  $output .= '</div>';
  return $output;
}
