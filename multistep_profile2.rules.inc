<?php
/**
 * @file
 * Rules Integration for Multistep Profile2 module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function multistep_profile2_rules_condition_info() {
  return array(
    'multistep_profile_is_complete' => array(
      'label' => t('Profile is complete'),
      'group' => t('Multistep Profile2'),
      'parameter' => array(
        'profile' => array(
          'label' => t('Profile2'),
          'type' => 'profile',
        ),
      ),
    ),
    'multistep_profile_will_complete' => array(
      'label' => t('Profile will complete'),
      'group' => t('Multistep Profile2'),
      'parameter' => array(
        'profile' => array(
          'label' => t('Profile'),
          'type' => 'profile',
        ),
      ),
    ),
  );
}
